var https = require("https");
var config = require('./config');

function api_call(path, callback, data)
{
	var options = {
	    host: 'api.github.com',
	    path: path,
	    method: 'GET',
	    headers: {
			"Authorization": "token " + config.TOKEN,
			'user-agent': 'node.js'
		}
	};

	var request = https.request(options, function(response)
	{
		var body = '';
		response.on("data", function(chunk)
		{
		    body += chunk.toString('utf8');
		});

		response.on("end", function() {
			callback(JSON.parse(body), data)
		});
	});

	request.end();
}

module.exports = {
	api_call: function(path, callback, data) {
		api_call(path, callback, data);
	},
    user_repos_infos: function(username, picture_name, final_callback, data_cb)
	{
		var proj_info = {};
		var nb_call_finished = 0;
		var nb_call_need = 0;

		function picture_cb(body, data)
		{
			for (var i = 0; i < body.files.length; i++)
			{
				if (body.files[i].filename === picture_name)
					proj_info[data.name]['img'] = body.files[i].raw_url;
			}
			++nb_call_finished;
			if (nb_call_need === nb_call_finished)
				final_callback(proj_info, data_cb);
		}

		function picture_parent_cb(branches, data)
		{
			as_asset = false;
			for (var i = 0; i < branches.length; i++)
			{
				if (branches[i].name == 'assets')
				{
					api_call(branches[i].commit.url.replace('https://api.github.com',''), picture_cb, data);
					as_asset = true;
				}
			}
			if (!as_asset)
			{
				++nb_call_finished;
				if (nb_call_need === nb_call_finished)
					final_callback(proj_info, data_cb);
			}
		}

		function languages_cb(languages, data)
		{
			proj_info[data.name]['languages'] = languages;
			++nb_call_finished;
			if (nb_call_need === nb_call_finished)
				final_callback(proj_info, data_cb);
		}

		function repos_cb(projects, data)
		{
			nb_call_need = projects.length * 2;
			projects.forEach(function(proj)
			{
				proj_info[proj['name']] = {};
				proj_info[proj['name']]['descr'] = proj['description'];

				api_call('/repos/' + username + '/' + proj['name'] + '/branches', picture_parent_cb, {
					"name": proj['name']
				});

				api_call('/repos/' + username + '/' + proj['name'] + '/languages', languages_cb, {
					"name": proj['name']
				});
			});
		}

		api_call('/users/' + username + '/repos', repos_cb, {});
	}
};
