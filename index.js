var https = require("https");
const fs = require('fs');
var username = 'zer0nim';
var proj_info = {};
const git_api_tools = require('./user_repos_infos.js');


function user_callback(final_data)
{
	var orgs_proj_info = {};
	var cb_need = 0;
	var cb_done = 0;

	function final_callback()
	{
		orgs_proj_info[username] = final_data;
		let string_data = JSON.stringify(orgs_proj_info, null, 2);
		fs.writeFileSync('orgs_proj_info.json', string_data);
	}

	function orgs_cb(orgs, data)
	{
		function org_cb(final_data, repos_data)
		{
			orgs_proj_info[repos_data.name] = final_data;
			++cb_done;
			if (cb_done === cb_need)
				final_callback();
		}

		cb_need = orgs.length;
		for (var i = 0; i < orgs.length; i++)
		{
			git_api_tools.user_repos_infos(orgs[i].login, 'thumbnail.png', org_cb, {
				"name": orgs[i].login
			})
		}
	}

	git_api_tools.api_call('/users/' + username + '/orgs', orgs_cb, {});
}

git_api_tools.user_repos_infos(username, 'thumbnail.png', user_callback, {})
